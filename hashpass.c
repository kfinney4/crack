#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>
#include <ctype.h>


int main(int argc, char *argv[])
{
    char passArr[100];
    FILE *fpr = fopen(argv[1],"r");
    FILE *fpw = fopen(argv[2],"w+");
    int c;
    int i;
    for (i =0; i < 100; i++)
    {
        memset(passArr,0,100);
        int index=0;
        while (1)
        {
            c = fgetc(fpr);
            if (c == '\n')
            {
                break;
            }
            passArr[index] = c;
            index++;
        }
        
        char *hash = md5(passArr,strlen(passArr));
        hash[strlen(hash)] = '\n';
        fwrite(hash,1,strlen(hash),fpw);
        free(hash);
    }
}