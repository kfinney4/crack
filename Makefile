hash: hashpass.o md5.o
	clang hashpass.c md5.c -o hashpass -l crypto
	
hashpass.o: hashpass.c
	clang -c hashpass.c

md5.o: md5.c
	clang -c md5.c
	
hashes: hashpass
	./hashpass rockyou100.txt hashes.txt

wordhashes: hashpass
	./hashpass words.txt wordhashes.txt
	
clean:
	rm -f *.o hash
	
check: 
	valgrind ./hashpass rockyou100.txt hashes.txt